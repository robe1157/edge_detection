{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Link to this document's Jupyter Notebook](./0225-HW2_OMP.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are two due dates for this assignment. First, you need to set up your assignment git repository on or before **_February 1st_** so your instructor can test and make sure everything is working as expected.  Second, you then need to complete the assignment instructions and then add/commit/push your files to your git repository on or before **_11:59pm on Thursday February 16_**. Your instructor highly recommends committing early and often. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Homework 2: Image Processing using OMP\n",
    "\n",
    "In this assignment, you are going to modify and improve the processing speed of an edge detector for grayscale PNG images. Edge detection is an image processing technique to find boundaries of objects in images. Here are an example image and the detected edges:\n",
    "\n",
    "**Note:** this homework was motivated and adapted from: https://web.stanford.edu/class/cs315b/assignment1.html\n",
    "\n",
    "<table>\n",
    "    <tr><td>\n",
    "<img src=\"https://lh3.googleusercontent.com/KAUNwCSj6OGz-XCnH8vtA0zoxtiWhlw67IYzULXvfH37ap5JYz46i8TMU-mM4x7-dkOuc3hgUA=w740\" alt=\"Example image of Sparty\"></td><td><img src=\"https://lh4.googleusercontent.com/goBSqfFUUXwKaeVBJEm3zdwzQZLokNZBTEbDx8GE-axIB3EaLURys7bcFhPT89r2CKN0e87BHQ=w740\" alt=\"Example edge detection taken of the Sparty Image\"></td></tr>\n",
    "\n",
    "The files necessary for this assignment are found in a git repository. To turn in this assignment you MUST 'fork' this repository on the MSU github account and add your instructor \"appeloda@msu.edu\" and TA \"rotemami@msu.edu\" to your forked repository so they can grade it. Use the following instructions:\n",
    "    \n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** On or before **_February 1st_** Fork the instructor's repository, set the permissions, clone your fork to your HPCC account and make sure you can compile and run the software.  Note, this program uses the libpng library which is already installed on the HPCC. \n",
    "    \n",
    "1. Navigate to the [Edge_Detection git repository](https://gitlab.msu.edu/colbrydi/edge_detection) using your web browser and hit the \"fork\" button (upper right corner) and fork a copy to your personal namespace.\n",
    "2. Invite your instructor to be a \"member\" of your forked repository by selecting the \"members\" setting (lower left) and inviting entering their email (appeloda@msu.edu, rotemami@msu.edu) and setting the role to \"Reporter\". \n",
    "3. Change your \"Project visibility\" setting to \"private\" which can be found under \"settings\"-->\"General\" and clicking the \"expand\" button next to \"Visibility, project features, permissions\".\n",
    "4. Copy the URL for your forked repository and paste it to the following online form on or before **_February 1st_**(so your instructor can test permissions):\n",
    "    [Git repository Submission form](https://forms.gle/yT1NbPSVK6S37BRG9)\n",
    "5. Clone your forked repository on the HPCC and work from there.\n",
    "6. Change to the repository and run the following commands to verify the code is working:\n",
    "```bash\n",
    "make clean\n",
    "make\n",
    "make test\n",
    "```\n",
    "7. To complete this assignment commit all of your changes to your forked repository on or before **_11:59pm on Thursday February 16_** \n",
    " \n",
    "**_Note:_** if for any reason you can not figure out git, please go to your instructors office hours. \n",
    "\n",
    "\n",
    "### Goals for this assignment:\n",
    "\n",
    "By the end of this assignment, you should be able to:\n",
    "\n",
    "* Practice using Git\n",
    "* Debug and benchmark existing workflows.\n",
    "* Using OpenMP to run code in parallel."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Code Description and Background \n",
    "\n",
    "There are lots of edge detection algorithms but in this assignment, you will work on an algorithm with the following three steps:\n",
    "\n",
    "1. Image Smoothing\n",
    "2. Gradient calculation (Sobel Filters)\n",
    "3. Edge Thresholding\n",
    "\n",
    "The following section describes each step in detail."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 1: Smoothing (Average vs Median Filtering)\n",
    "Before starting the actual edge detection, we smooth the image to reduce undesired edges detected due to noise. There are lots of ways to smooth an image. The code provided uses an Average Filter.  Average Filtering approaches uses a \"sliding window algorithm\".  The input and outputs of a \"sliding window algorithm\" is an image.  The value of each pixel in the output image is calculated using a \"window\" of data around the corresponding pixel in the input image.  \n",
    "\n",
    "For the **Average Filter**, all of the pixels in the window for are averaged and the value is stored in the output image at the same location as the center pixel. \n",
    "\n",
    "If you think about it, the sliding window is not fully defined at the edges.  For the Average Filter this is not a problem. The algorithm just takes the median or average of the valid points within the windows. Here is a snip-it from the code:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```C++\n",
    "//Average Filter \n",
    "for(int c=0;c<sz.width;c++) \n",
    "    for(int r=0;r<sz.height;r++)\n",
    "    {\n",
    "        double count = 0;\n",
    "        double tot = 0;\n",
    "        for(int cw=max(0,c-halfwindow); cw<min(sz.width,c+halfwindow+1); cw++)\n",
    "            for(int rw=max(0,r-halfwindow); rw<min(sz.height,r+halfwindow+1); rw++)\n",
    "            {\n",
    "                count++;\n",
    "                tot += (double) img[rw][cw];\n",
    "            }\n",
    "        output[r][c] = (int) (tot/count);\n",
    "    }\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Gradient calculation (Sobel Filter)\n",
    "\n",
    "Assuming that each object consists of pixels that do not greatly vary in their brightness, the edges in an image are from the sudden changes of brightness in the pixels.  One way to measure how big the change is to calculate the gradient magnitude at each pixel. The gradient operator we are going to use in this assignment is Sobel operator. Sobel operator is based on the following two 3x3 filters, which calculate x and y component of the gradient, respectively:\n",
    "\n",
    "$$ G_x =\n",
    "\\left[\n",
    "\\begin{matrix}\n",
    "    -1   & 0 & 1  \\\\\n",
    "    -2   & 0 & 2  \\\\\n",
    "    -1   & 0 & 1 \n",
    " \\end{matrix}\n",
    " \\right]\n",
    " G_y = \n",
    "\\left[\n",
    "\\begin{matrix}\n",
    "    1   & 2 & 1  \\\\\n",
    "    0   & 0 & 0  \\\\\n",
    "    -1 & -2 & -1 \n",
    " \\end{matrix}\n",
    " \\right]\n",
    "$$\n",
    "\n",
    "Here is a code snip-it that creates the filters:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```c++\n",
    "//Sobel Filters\n",
    "double xfilter[3][3];\n",
    "double yfilter[3][3];\n",
    "xfilter[0][0] = -1;\n",
    "xfilter[1][0] = -2;\n",
    "xfilter[2][0] = -1;\n",
    "xfilter[0][1] = 0;\n",
    "xfilter[1][1] = 0;\n",
    "xfilter[2][1] = 0;\n",
    "xfilter[0][2] = 1;\n",
    "xfilter[1][2] = 2;\n",
    "xfilter[2][2] = 1;\n",
    "for(int i=0;i<3;i++) \n",
    "    for(int j=0;j<3;j++)\n",
    "        yfilter[j][i] = xfilter[i][j];\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once x and y components of the gradient is computed, the magnitude can be computed by: \n",
    "\n",
    "$$IMG_{Gradient} = \\sqrt{G_x^2 + G_y^2}$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```C++\n",
    "// Gradient Filter\n",
    "for(int c=1;c<sz.width-1;c++)\n",
    "    for(int r=1;r<sz.height-1;r++)\n",
    "        {\n",
    "                double Gx = 0;\n",
    "                double Gy = 0;\n",
    "                for(int cw=0; cw<3; cw++)\n",
    "                    for(int rw=0; rw<3; rw++)\n",
    "                        {\n",
    "                                Gx +=  ((double) output[r+rw-1][c+cw-1])*xfilter[rw][cw];\n",
    "                                Gy +=  ((double) output[r+rw-1][c+cw-1])*yfilter[rw][cw];\n",
    "                        }\n",
    "                g_img[r][c] = sqrt(Gx*Gx+Gy*Gy);\n",
    "        }\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Edge Thresholding\n",
    "\n",
    "The last step in the code provided is edge thresholding.  In this case, edges are defined based on the gradient image ($IMG_{Gradient}$).  Values over the threshold are considered edges and vales under the threshold are not edges.\n",
    "\n",
    "$$IMG_{Edge} = IMG_{Gradient} > thresh$$\n",
    "\n",
    "The threshold (```thresh```) may be varied to get different results.  For the images provided set the threshold to be 50. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```C++\n",
    "// Thresholding\n",
    "for(int c=0;c<sz.width;c++)\n",
    "    for(int r=0;r<sz.height;r++)\n",
    "    if (g_img[r][c] > thresh)\n",
    "        output[r][c] = 255;\n",
    "    else\n",
    "        output[r][c] = 0;\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Homework Assignment\n",
    "\n",
    "For this assignment you will do the following parts:\n",
    "\n",
    "1. Establish Serial Benchmark\n",
    "2. Optimize the Serial Code using compiler options\n",
    "3. Modify the code to use OpenMP loops\n",
    "4. Final Report\n",
    "5. Deliverables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1: Establish Serial Benchmark\n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** Benchmark the code provided using the image files provided in the images directory.  This file should provide a solid baseline.  Make sure you explicitly describe the hardware you ran on and run enough trials to account for hardware and system variation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Ran for 10 tests on the HPCC\n",
    "\n",
    "AVERAGE TIME: 0.2357\n",
    "\n",
    "2. dev-amd20-v100 node\n",
    "3. 128 Cores\n",
    "4. 960GB Memory\n",
    "5. AMD EPYC 7H12 64-Core Processor @ 2.64GHz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 2: Optimize the Serial Code using compiler options\n",
    "The code provided does not optimized very well.  There are lots of things that could be done to speed up the existing code. For this homework I would like you try and benchmark two optimizations:\n",
    "\n",
    "1. Use different levels of compiler optimization \n",
    "1. Change the order of the loops (row and column) for the Average Filter, Gradient Filter and Edge Thresholding. See if the change in order will increase speed due to multiple cache hits. \n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** Make the updates to the serial code as described above.  Benchmark and compare the time differences between the original version, the modified versions.  Pick the changes that provide for the fastest code.  Make all comparisons on the same hardware as Part #1.  If possible provide a detailed table or graph to show the differences. \n",
    "\n",
    "Make sure you also save a version of your best serial code to turn in. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use Different Levels of Compiler Optimization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. GCC -0 ==> 0.2565s\n",
    "2. GCC -1 ==> 0.3062s\n",
    "3. GCC -2 ==> 0.2903s\n",
    "4. GCC -3 ==> 0.2802s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Change the order of loops for the Filters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Average ==> 0.3078"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 3: Modify the code to use OpenMP loops\n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** Rewrite the code to use OpenMP loops to run the Average Filter, Gradient Filter and Edge Thresholding in parallel.  Make sure you carefully note the types of changes you make and use proper synchronization.  Benchmark your changes with different OMP Loop Scheduling options and compare to the serial benchmarks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Average ==> 0.0753s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### OMP Loop Scheduling Options\n",
    "Watch the following video which talks about ways to play around with schedulign options:\n",
    "\n",
    "* &#9989; **<font color=red>DO THIS:</font>** Watch the Scheduling [Introduction to OpenMP: 10 Discussion 4](https://www.youtube.com/watch?v=8jzHiYo49G0)  video:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "image/jpeg": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2MBERISGBUYLxoaL2NCOEJjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY//AABEIAWgB4AMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAEIQAAIBAgQCBgcFBwMDBQAAAAABAgMRBBIhMQVBIlFhkZLSBhMXVHGB0RWhscHwIzJCQ1JT4RQW8TNicgc0grLC/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAgEQEBAQEAAwEBAQEBAQAAAAAAARECEiExA0ETUUIy/9oADAMBAAIRAxEAPwD5+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADsPZxxj3nA+OflHs44x7zgfHPygceDsPZxxj3nA+OflHs44x7zgfHPygceDsPZxxj3nA+OflHs44x7zgfHPygceDsPZxxj3nA+OflHs44x7zgfHPygceDsPZxxj3nA+OflHs44x7zgfHPygceDsPZxxj3nA+OflHs44x7zgfHPygceDsPZxxj3nA+OflHs44x7zgfHPygceDsPZxxj3nA+OflHs44x7zgfHPygceDrav/p7xWlFynisCktf35+U0tbglahK0sRh2+yUvoDGsBclw2rFJ+sptdab+gfDqqS/aUnfqb+gFMF1cLrtXzQ739A+F10rtwXzf0BikCzLA1Y7uP3nqwFVq6lDvYFUFtcPrPnDvZkuGVn/ABU+9/QCkC/HhNeV7Tpadr+hnT4LiJuyq0U+pt/QGNaDZrgWLc8manf4v6HsuAYuDSnOjBvk5P6BcasHS4T0J4jjKanSxGDSfKU5L/8AJaX/AKc8Xe2JwPjn5QjkAdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UDjwdh7OOMe84Hxz8o9nHGPecD45+UD6gAAAAAAAAAAAAAAAAAAPCKvXhQg5TdkjHE4qGHi3O9krtnJcZ46q79TSaWvWRZGHpHxt1XKhQk8nOW1zk6snUlu3YmxVVylJvfe5Tz7tb9oWsm7aO/wJ8OoK7nFS7GQxqSe6+8lp7pc2BNOcmujaJBJ6ayT+ZO6WrcpW7CJuN7L72BGo7EkY22ewho/wDBZjGLQEWS/wDhE0YNLpJrtMoQUOk2Q1K7b0T+ZFWKdSEJXio362l+DPYuMnmuu6xTUm3qiVR2aAmz5ZNxu11Hik1WWWSjzV+ozpxV+yxhWdmnHfYLjecPrTpwVSeIi2laOaai/lmsjpMBjliFdyi3z6S/I4GOsLqMJyWrTvcu8L4m6GJjPRRbWZRVkwfX0NGRXw9aNWlGpB3iyc05vQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI6tRU4Sk3ZJGZpeL4tUoNuVtP1+voFjT+kPFJRhKnms5LZdRysK0puU5PlZGfEsVLEVHKTb157kFKLdO7WhlpFWi95OyfeyNPqsuwlrZrpMi1T1epWXqU5vRXLWGp2avZy6+ohhK260LFOaVtdF94VI43XSe/IhkorldfEmivWWZHKOeT00uRWVBrlp8SxZPREMI+rV2kuxlhLNHo3u+0LIjqzVOGqd+3cqxhn2uXFhvWS1SfxZK8HKMrZbIavjqvRot6PWxap4a7LNDC7N7lylTUV+7dmLXTnlReHUYvTSxUqwcZXN46KlvuUcbhrJuOonS9celB1p03Fxbi1tlLPr6ldKVaU5u3Od/xIYpyg4u603RVUHCdurmjbhXaejldwhLDykpRvem/wAjok72OAwEKkUpxk1KLvfMll/M7Xh+IjicNGaabtZ6liWLh4NgVl6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACOtNQg23Y4L0kx8qleUVor2tbqO1x8/V0nLq1/A+ccaT/1Us11rbUlajXVp20vdvmybCrPSaXVfUqV30821+SLGDk3Gy6wFRKKvzKyVr33L9WKc5SS6KWncUpQcpN/MDKFviSat68iGCVla/cSxXWBPQmtY89ielRlUneK0RVpRyVFZM2+FTbjGO7tczbjpzzqKOHnNtu6RYo4LM0mt9rmzpQWy+RYw9Nb7nPydp+arR4ZKnGLVt9UWY4BvWSStqXL6dnYZxknG9nca14qyw0Yq6SI5U8uxalNELZl0kQPZkM4Kad+ZacSBpp8iJY1GIoujO8NYs19VZaum3UzfYyknG5p8ZSUXmT3W99jtzdjydzKyo1V/FyfcdTwTiC9aqd/3ltu+/Q42NR7tJPa6NrwjFZK0YqSXLVK5piPoEZKSuj0gws1OindP4E5ph6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHgGs4zK2HltZRbZ854nWdfEyk292zv+PSUaE87eXqXYrnzut0qz+JGv4q1OkrpaJEmHVmt9mz2drXS3ZhCWWXw3CLEZNpJvZO4cdElu7ENGXSu977FyMVKSX/cRqe0FKldvV2TMvUybvokXIUr3st2yWlh2n1/IzrpOFSlTmuTa7TdYOnZXMaGDdSWaWy5Gyp0oxtoY661344x7RjLZaItUoxtdsjyvZWVjJU3fpNsw64kdSCVkzJVIWtqYLLB22M1LTYqYwk4sjcV1Gd291c8Ut9CKxSexHJK+xNK3wZFLX4hKrVtE+o0mKsqrurrsN3iGldbGjxqTk3F6P7jrw836Ka6Mnq2uwuYWrUulGWW3aVIKSeq35ktP97WSt1HRwju+D1nUo63i1vpubdM5fgMmowanFtOzR01OWZXEOmZ6eHpWQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADw9PAOb9KJKFN6Xvr+RxE4JSk73uz6D6SUfWYKVrXastPn9TgsXBwik9yVqKkpJyfR2IHpdK99iVqzsnZHkU23d94EdJSzpcuZscLJy1Kag9ld/mbHDUckNdzNb4ntcpRSsXaairXWpVpx0VuRYp3TSOVerlbpXZZjdEFNPYswWmrI6xlLbQJ9TPXqrWPGrK4HrXSTMlqiNzWlySM4uN07WBWLYvrcyklLW6IpPKt7hNZSfZqRvcObaMVdvWxErCpFyRpsdQyzvr3G8ZBiaSqwaaTNy45d865v1dtb2R5FPMnFLXdosYqi6bavYhpwnFSctO06vLZlbvglacXJ7qx2eGmp01b5nC8EqJV0rNX0dzt8Gn6pZk01oWF+LJ6eHpWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADw9PAKHFIesw8o7t6JHz3iCcKtWDd3FtfM+lV1HK7tLR8z5xxGLWKrZla0tiVqNXVV+X3CEWort1seyT6c/6dl1tmcIXmrO+VXYEuFptayNhTjdKyKtKxapzyo5135W6aUV2ElFKclK+hWjV67a9pI66hBt1Ir4JtmXTykbDPCG71EcQr6M00sTNO98q62erG3WjuMXzdBGqmjJPNpY52jj5Kuk27G/w7urtmbMb5615V0jY11erOOsJK/UbHGJuNomlrU5a3uyxOnix9RS6Tbt1MnhjIS/mST6pFFYam5XmpTa3S5GVKrhI1IwdKd23a80tvmazXO3PtbKnim5Wgl8UWoOXM11HF4f8AltrsnH8y7TxNOron0lyM2NSp8y5njPFZjruRVTG4eFWi9E2aRx9Wui+R0dR9E1FWmnVclbfVPY6cVw/Se3vC5Tp41Ss3BM77COLoxcJXRxVKEaeXJqmzpuC1JSi0/jobl9udnptweXBpzegAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeHoAirQzW121OC9IsPVp42U5K0ZartPoE9t7HE+kmKjXxjpRdlT5W5slWOWxCazp7t3RYw0G4frU9rRUpuWit2bFjD5VTTXMla5+sbZdFyMoy62ZTjdt9ZhbXsRl1ZKOZvM9Osl9bh6Mek0vgrsqr1larli8sVsXIYBON8yzc77DF9/wpVqeJdP1NJSz6XlLVfHqJsRg3S/6uHdN/1RV0TYLDQwsozll6OsVFaXJsVX9Ynmk5K+1yXP41zL/wCmllTtJSW5vMLUeRGpqa1O17mywqeVXM9Ov5z2uznmRXqU/WRs2opfeS20HY9TLfXKPC+qpXSgsveyri+GKvUvTjCUW7rNpYu/6dN5loSQo3teVvianWOXXEv1Uo4KNKk4ytKcuaEeHxbUoPJJbNGxVGKWj7j1RWy3F61ZzJMivSozX7zV11Hs42ZZSsR1LNGFVmr/AAKFeh+3eWyfWbBrV9RSxcrOM7aJ2OnLn+k2JKdFpZs3cb3gMJRc77Ja/E02Gk2uo6PhEMuFvveRqfXPv1wvg9PDo870AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABjJXjY4LjkXHjGKWyuuW9zvjkfSjCyp4pYm37Oosrfb+vwJWufrm5Q6LSVtNTKk3pfqPZPRpW2MY6LtI3mVYgs07MtLCxa62VqU/lqXqU9bs5125QRwtp3yliEJK3RSLClm0Vj1pLQzrrOETjp022+pFeussXy7C7lzfAoY+aUlTiItmKcHesja4d9E1cUvWJI3GGpLIrvUta/NNF9HQ8ukSVsOoKElO6aI7RtqyN2ypKSu9NSREEX6uV1qWKU41U76SJiPVHruZNKMRbkuRhKXIIkvoQylc8ctDFvUM2I53TuirUWa6tzLjW5SqN+v3+RqOfXxYw1KcUk1/k6ThTX+ksntJo5+hUnKCUXZ8mzfcITWEb3vNmufrH654L4PD06vIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPCrjsJDFYWdKonJSVvgWzFq4HzfGYOeCxFSlVteL35NcmiDRyZ1vpZg4vBwxUYrNTkoyfY/8/icm1vJv9fqxmuu77e030tSzCpyuVIb6GcZWM1vm42NKpZlqGsrmrpyehsaMrnOvVzU9SXq6Tkc9Vrt15Nm8xkstHWxpJ0lKV7OzZeWf0qm8TONe6jeK53N1hsepQTvqa54WM5rdPkkixRwNTI5p2j12N3HLnqytm8VKcUoWbe1ypLC4qpW9ZVxLSW0Y7GeEhVimk07dRLKTvK9pPqMunlL9S0JtWzP4Eibp1FJbMipz0ytWa5XMHiNXF62M2NTqNmp5lcxm9CKhNOle/IkaMt6ju7vqPb6XHMwnPK1fYrNZtpIoVP/AHKkna3IkqVnZJvnuU6knnaW7lexqOPdbfCWlT6a06zoeGQyYKGt73f3nM4acVBxT7Tq8JTdLC04PdRVzfP1z/T/AOYmAB0ecAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwCDG4eOKwlWjPacbHzrFxdKu6MtJReVrtPpj2OA9IqWTitbe7f5ErUauDtMmWqv+BXX7sdVdsmoz/gepiusqWm+ki/QlZrma6NuW5ZjV9XFt6aGa78VlxHEXqKEXolqeerjminrbV6dhXVGdVNu6k2nb9fAkr06zvmT0urdRZHLq7ViMYXf9OVfL9al2OT1Xq6ck422/XwNJFwoZm3mb3uz2HE4Qk7PUYs5/62lFZJq172v+RHUitWoS7ilPjFLS87v4mMOP06bsm18roZWs5/6uVKsYyiumm3q7fIjnJVctRKztaS6mT0OI0MSlmUX2o8nQheXq5aPl1ES858ZYao6dJZrrXYvRqKWnMpxqJ/vRu0rO/wCvkT0ssoqUW3Zc99GZsb46/iRy2d+epWrTs11X1RnJu01y3K8m3K2jS3+Ai9V7BxbV7WvYrVLuat8H2Ga0Wm0es9km7Jrlrb9dhuOXTcejuFjX/a1FdRtaPadOjnvRl2lUiupXR0J0jz9X29ABWQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPD08AHIelWFkpKrZdJW25pv6nXmn9IKefBNWu7hY4CV1I8Td4J9d2S4iHqpyvvf7iBN5kk9FuzLcW4y6XLUmjZzWf91K7tzIKcstNu2rStotDGUpN3u+0xjpvpssL043b53/XcWXDNTktG5bFSnlUFKO99n2K5Zp1H6uPLmStcNHjKFdV1Saai9c3WS0ODxau1cvVWqlWN/nclp05NdGWX4GtdeeJaqvhFNW0RJ9kYW37SWX5FuNGo1Z16lnrbMSQw8cyvImunjGpo8LhHEqUHJQXLrNrHDxoQzRTfzJ7QhomeO8odF79pm1myT4qNXSkrWeidtv1+RJCUKdam4puE7qSfJ/pkSm6U7Sejl95HGThXctlysWOF+rSdp2b0X4EDVozS67pk1C0+klo18Lu5DUTUJdewi2vLKUb7aLvJIU2t9U9UR0YdDK272zMnozzVYxfXqajFbvgVL1dJyy2cuf5G7WxUwNPLQSStpsW0dI4X69AAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKuOpRnQk2m+wtGMoqSswPm3FaXq8VVjlayN9xrlFxlN3d+R1HpFw506jqx1vq/g9DmsQ/3nzTasRuJqcs0I66WbfxMpOKSTa7V28ilGs4xcd1ck9c3GLTs3o2RdXqVRpwpvSSWps6UZKOe3RXRRqMB+0xMI7pRbfyN05JRtD+FdfN/8mLG+ar+q9ZPNa1upFmlhpa6di+JFTqNUWrdK6/MvYdaUruyUrv5f5I35VjSwyc5xWttEewpJ2lHZbolc1Gr0NM3N8tD1QyX69cq/ILtVK8PVKNSOsU9TCnNZ4q+j0JKqdmlrm2XU7FWMW0tNb6GV3EteiqvR0T/ADNfWWSVrNT21Rs4VI5stSLUnz5EOKyypqTV4t/NG5HLq+0VNqMFON1a2nWeyedVHvdXTIKVRpJXu72+8zU+g1Hrshi76S0nqprVNJW+ZZoUJZnLmlr935GGHilBRX8L1/X63MsZOcMJNwVnlb+H60NMVuPRviMMXQnSv0qb0fWn+mbs+a8BxcsHiXNXSeZW6ua/I+gcOx1PHYaNSD6VulHqZpzq2ACoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHh6eAU+I4OGKws4S33T6j55jaMVUlZaO9u3tOr496S0aMamFwlqs2nCVT+GLtt2s4bHY2vKo5yqNtq7IsYzhaaXJHrWjSvZu6MlJVqcaie65kcpZNE2n2EabPhSg661tJKz/ABZtnDNGVtG5JnO4GqqNSnLl1X3N9Rmp1dOkr5kuy3+RYSppU8700aWvy2M6Ms2eG0m0l+H5jNkg5t3u7O/4/cR003iVK2kl97t9DFdeampzzV27fva95Yq1rrSKuncrU1kjTlHTM7X6tf8Ag8qyyVmnve+nMy6eklWLWlrOKVvieKmpQzRVrtadTuYQqesg4WbeuX8SbDztKMKn8Wif6+JYz1Ude7i6sYJuO8bbNFGtNKmktFKV12O234m2jTUam6avqzSY6slJxjpGOr+L5G442qbqShGMVrLddmpbozjJWWjVv1+JSinUkrf8Gxw9Fxgm21Jj41mrdOaUJPTMtu4qY+TdGXSt0XdFiMcqKmPdsPN9hndbnONVwxWh82dX6L1VT4g4SkkqkGknzd1/k5rAQtTXeXlNwlGUXaUXdNcmbcX0UFDhvEoYzCwnJ5ajXSXaXk01dO6NMPQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMZSjBXlJRXW2BkebGpx/pBhcJpB+sn1JnL8U9JMRiXkpzcY88uiA63HcawmDi+nnkuUTkOMek2IxcZUaM8kXo8vJdRo8Riak9HJ2INo3IqSpUUZwhB9GG/a+bIsYr2+BEyev0oRfWBjgan7LI3pczxEbTcrFXDPLUlFl1SbVmRqe4xWqjZ620/A22Dxfq4NvVpafr4GpcbO6MqVXI0u/sKjpq03LPTg9m1b7zHM1Rozu731fzTNRTxU4tTi8zu03fct0cXGeaDajfk9rksWVfqVL0nBNrLJu3bqYVq6lXi9dY/gQyxDjGpmjn0vGSezIK9RZ1JXuldPs6iY15J44tp/wBLT79i1RrZ0pab6X5cvyRqHCTvLm9EuomjiVRp5NXr1jE3W1xGKglLLprc0NRyq5Yx5a/HtJJ1KmIbT/df8PUT0aNulYW4vPOssPQUbNJr5ltayuYRV/gTWtFWRz13kxjKVrK3SKPFujhknvN2NkoJz+BquKz9ZiowW0RPqdeojw0bQRI3dnkFlihezR1cFvC4mdCTUZW0ul2m0wfGsqblNo0MnfbexjbVtBHZYbj1GrpJfPY2dHFUa6/Z1E+y589WInGnkWx7GtUtpNq3VoXUx9GPTh8Jx7G4ZpSqesh1SN7hPSXC1Wo1k6cnz5DUxuwR06tOrHNTnGS60yQqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwr4jG0MMm6k1fqW4FkgxOLoYWGavUjHs5nPcS9JJu8ML0e3mcziMRVxFRzrVJSb62TVx0uN9Lownlw1LMtm5Gnx3HcRi475TVyjdGPK3MLjyc5Tk5Sk2yK1viSNGNgIZGD1VjOe+hiluBE1oSw6eH7UeSjoMO7TlF8wK0/wBnXjLkXe0hr07xJMLLPDK94kq8pDCUOrmSuLFiN4rrNC61SkZxqzTV9dCTL2aGOQup4pqeImo2jdJbq5LCtfloV0n8SSDJp4rSbknaSu1rqetKUUtraWI4Xs7cyammS1ucs6EEti3COmmxhCm+osRjrZd5iukhT0drXJcqsvxEI20JsvRv9xlpE3kpubskjQuTrYiU+tmx4tXy01TjvLc1+HjuzpzHL9L/ABMzxnifSPZNWNuTG+p45WSW10eJnj216l+AHq105kkVrZFdPK11E8LNPtA915BbHqTT7F8zxu621YE2HxmIw0k6NRxtsbvA+k801HGQzR/qitTnuxHsVoEx9Aw+Ow+Jgp0aikvwLCaex85hVnSmpU5OMlzTN1w/0gqU7RxKuv61v/kupjrQU8PxCjXScZqSfNFtSUldO6Ky9AAAAAAAAAAAAAAAAAAAEVbEUqKvUkl2czVY7jcKKspKC63u/kBuJTjBXlJJdpSxPFcPh4Xc12XOUxfHatW6pXT/AKnq2amrWnObnOTbe5NXHS4v0gVROKk0uxGnxGOlUdoylfe7ZRTvF9R7uyNMr3dyOTPbpOxhPRgJO9jB2TueyZjL90I8nq0j1qyCV2jOWwFW2p5BXTfaZtCkv2SfXf8AEowaIW8lVSLeUgrRAlcVJPtIYRdOpm7yXDSvp1EkqZCJUrxugqafI8w7cdHsW/VrcxXae1dUX1XCol2lTLMaK5omt+LV/wCnMo4dm1WGT5Jmaw3YNXxa2nQsWqdB7KLuXIUUlqTxglsjOtYr08O7dJ/JGeWKeisiy4qK7TDJeWiIuIsvNGcpKnTcpPYm9U3yt8TU8Xr5F6qLu3uWTUtyNXiKrr1nLuJqSy0/iV4xLVrRS6jq819vNF8TC2bd/IyaueWKjxvVaGL5fBfgZNdIx5/JAYSRJSd0Yy2MqIE17GDauZMiluBkz22ll9xhyM4gG9d2viZZtGQ1JWkjLM2gJ6VadHWEmuuzN5guMZcsfWWstE9jnE77maeqdwO6ocWoVLKUkn2O5ejOM1eLTR89o4irQ0hNq+tuRt+H8aVN5ajcNNXvdl1nHWgp4fHQqJZmlfZ3LadysvQAAAAAAAAABhUqRpRcptJI03EeOQopxjLL/wDZnAYr044rir544eP/AIxkrfea2fHcVOV5QpN/B/UiutxXGa1W/q+h/wBz1ZrZ1HOWaUnKXW2aH7YxHOFJ/J/UfbGI/opdz+ow1vVcxm7Qb7DS/bOI/opdz+p5Li+IlFxcKWvY/qMXW9p/9OPwMkaFcaxCSShS7n9T37axP9FLuf1GGt0/3hM0n2ziL3yUu5/UPjOIf8FLuf1GGtuw9rGmfFq7/gpdz+o+16/9FLuf1GGtzTJZrQ0MeLV47Qpdz+pk+M4h/wAFLuf1GGtpU0g2e01+yiuxGnlxWvKLi4U9ex/U9XF66/gpdz+oxNbi1yKrFKJrftfEf0Uu5/UjnxOtPeNP5J/UYa3GGgovPyRZtr8TQx4tXjGyhSt8H9TJcaxKt0KXc/qMXW8SyyvY2FCCaST0Zyn23if7dHuf1JaXpFi6SSjSoO3XF/Uzedb57kdhToNci3TovqZxsfS/Hx2o4Xwy8xIvTXiK/kYTwy8xjwrr/ry7WNBckeqicV/vjiXu+E8MvMev044k/wCRhPBLzDwp/ry7P1T6jONPW92cO/TbiT/kYTwy8x7/AL34n/Zwi/8AjLzDwq/7cu79UurXtPYUklbS5wf+9+J/2cJ4JeYL034kv5GE8MvMPCp/ry7yvJU6TdraHI4up63EyfaazE+mHEcTFxnTw8U/6Yy+pQ+2cRe+Sl3P6muebGOv0l+OgiuRMzm1xzEranR7n9T37exX9uj3P6msc9dCEznvt3Ff26Pc/qPt3E/26Pc/qMNjoHuYS3XwND9uYn+3R7n9Tx8bxL/gpdz+ow2N9LYyonPvjeJf8ul3P6nseOYmO1Oj3P6jDXRsjktTQ/b2K/t0e5/U8+3cT/bo9z+ow2OgienPLjuJX8uj3P6nv27iv7dHuf1GGxuqms4omS0OdfGsS5KWSlddj+pl9vYr+3R7n9Rhre3szNO5zr43iX/Lo9z+p7HjmJjtTo9z+pcNdI2R36SjrruaF8exT/l0e5/UxXHMSpZvV0b/AAf1GGx1lHGV8NNunNq+/adBwvjKqLLLTri3+B81/wBwYv8At0fC/qex9IcXF3jCin8H9RibH2enUjUgpQd0zM+T4b084thlaNLCy/8AKEvMWPaPxj3bA+CfmKy+oA+X+0fjHu2B8E/MPaPxj3bA+CfmA+oA+X+0fjHu2B8E/MPaPxj3bA+CfmA+oA+X+0fjHu2B8E/MPaPxj3bA+CfmA48AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/2Q==\n",
      "text/html": [
       "\n",
       "        <iframe\n",
       "            width=\"570\"\n",
       "            height=\"360\"\n",
       "            src=\"https://www.youtube.com/embed/8jzHiYo49G0\"\n",
       "            frameborder=\"0\"\n",
       "            allowfullscreen\n",
       "            \n",
       "        ></iframe>\n",
       "        "
      ],
      "text/plain": [
       "<IPython.lib.display.YouTubeVideo at 0x7f30544d08b0>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo(\"8jzHiYo49G0\",width=570,height=360)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">**schedule(static [,chunk])** Deal-out blocks of interations of size \"chunk\" to each thread\n",
    "> \n",
    ">**schedule(dynamic[,chunk])** Each thread grabs \"chunk\" Iterations off a queue until all iterations have been handeled.\n",
    "> \n",
    ">**schedule(guided[,chunk])** Threads dynamically grab blocks of iterations. The size of the block starts large and shrinks down to size \"chnunk\" as the calculation proceeds.\n",
    "> \n",
    ">**schedule(runtime)** Schedule and chunk size taken from OMP_SCHEDULE enviornment variable (or the runtime library).\n",
    "> \n",
    ">**schedule(auto)** Schedule is left up to the runtime to choose (does not have to be any of the above)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Static ==> 0.1554s\n",
    "2. Dynamic ==> 0.0977s\n",
    "3. Guided ==> 0.1117s\n",
    "4. Auto ==> 0.0882s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 4: Final Report\n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** Write a report describing what you learned (There is a template in the instructor's git repository).  This report should be in the form of a narrative (story).  Start by describing how the provided serial code performed and what you did to accurately measure that performance. Then talk about what you did to optimize the serial performance. Finally, describe what you did to add in OpenMP and how the different scheduling options changed the speed of the algorithm.  Make sure you include well labeled graphs of all of your benchmarking data and explain the graphs in your report text with a focus on any odd results you may see.  Conclude with a general lessons learned.\n",
    "\n",
    "This is an image algorithm. You should include example images. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Grayscale PNG images\n",
    "The code provided is desinged to work only with grayscale ```*.png```  images. Most PNG images are color and many images are in different formats (ex. TIFF, jpeg).  Fortunatly there is an easy to use command called ```convert``` which comes with the [ImageMagick](https://www.imagemagick.org/) Library. Feel free to download and install the software for your own use or use the software already installed on the HPCC.  The following examples can be used on the HPCC to load ImageMagick into your PATH, download an image from the internet and convert the image to a grayscale ```PNG``` file:\n",
    "\n",
    "```bash\n",
    "module load ImageMagick\n",
    "wget https://c1.staticflickr.com/7/6118/6369769483_882a704a38_b.jpg\n",
    "convert -colorspace Gray 6369769483_882a704a38_b.jpg MSUStadium.png\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 5: Deliverables\n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** Prepare your files for the instructor.  I recommend having three versions of the code; original serial version, optimized serial version, optimized OMP version.  Update the provided ```Makefile``` to build all three executables. The files should include.\n",
    "\n",
    "When you are done, add/commit and push all of your changes to your forked git repository. Your instructor will use the following command to compile and test your code on the HPCC:\n",
    "\n",
    "    make clean\n",
    "    make \n",
    "    make test\n",
    "\n",
    "Watch the following videos for some hints to help streamline your code and benchmarking process:\n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** Environment Variables in OpenMP [Introduction to OpenMP: 11 part 4 Module 6](https://www.youtube.com/watch?v=WXqicIq68-s) \n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo(\"WXqicIq68-s\",width=570,height=360)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 6: Optional Bonus\n",
    "\n",
    "&#9989; **<font color=red>DO THIS:</font>** Create an updated version of the Edge detector which includes some improvement. For example:\n",
    "\n",
    "1. Use a median or Gaussian filter instead of the average filter.\n",
    "2. Use Non-maximum suppression instead of thresholding.\n",
    "\n",
    "Learn more about the above options [here](https://web.stanford.edu/class/cs315b/assignment1.html) or pick be creative and try to make something else. \n",
    "\n",
    "Make sure the updated code also runs using OpenMP and include it in your Makefile with deliverables.  Include the improved edge detection examples in your report. \n",
    "\n",
    "Here is a short video about Median Filters. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo(\"3ELsNs4ERY8\",width=640,height=360)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-----\n",
    "### Congratulations, you are done!\n",
    "\n",
    "Submit your tgz file to the course Desire2Learn page in the HW1 assignment.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Written by Dr. Dirk Colbry, Michigan State University\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-nc/4.0/88x31.png\" /></a><br />This work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc/4.0/\">Creative Commons Attribution-NonCommercial 4.0 International License</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
